package runner

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"os"
	"path/filepath"
	"testing"
)

func TestRootReleaseFileProvider(t *testing.T) {
	require.Equal(t, "/release.yaml", rootReleaseFileProvider())
}

func TestProjectReleaseFileProvider(t *testing.T) {
	require.Equal(t, "/project/gitlab-runner-operator/hack/assets/release.yaml", projectReleaseFileProvider("/project/gitlab-runner-operator"))
}

func setupRootReleaseFile(t *testing.T, contents *Release) string {
	f, err := os.CreateTemp("", "")
	require.NoError(t, err)
	require.NoError(t, f.Close())
	defer f.Close()

	mockRootDir := filepath.Dir(f.Name())

	f, err = os.Create(filepath.Join(mockRootDir, releaseFile))
	require.NoError(t, err)
	defer f.Close()

	if contents != nil {
		b, err := json.Marshal(contents)
		require.NoError(t, err)
		_, err = f.Write(b)
		require.NoError(t, err)
		require.NoError(t, f.Sync())
	}

	return f.Name()
}

func TestFindReleaseFile(t *testing.T) {
	rootReleaseFile := setupRootReleaseFile(t, nil)

	mockRootDir := filepath.Dir(rootReleaseFile)

	mockProjectDirSuffix := filepath.Join(mockRootDir, "project", "gitlab-runner-operator")
	err := os.MkdirAll(mockProjectDirSuffix, 0755)
	require.NoError(t, err)

	f, err := os.Create(filepath.Join(mockProjectDirSuffix, releaseFile))
	require.NoError(t, err)
	require.NoError(t, f.Close())

	mockProjectDir := filepath.Join(mockRootDir, "project", "gitlab-runner-operator", "something-else")
	err = os.MkdirAll(mockProjectDir, 0755)
	require.NoError(t, err)

	f, err = os.Create(filepath.Join(mockProjectDir, releaseFile))
	require.NoError(t, err)
	require.NoError(t, f.Close())

	tests := map[string]struct {
		rootProvider    func() string
		projectProvider func(string) string
		expected        string
		err             string
	}{
		"root file exists": {
			rootProvider: func() string {
				return filepath.Join(mockRootDir, releaseFile)
			},
			projectProvider: nil,
			expected:        filepath.Join(mockRootDir, releaseFile),
		},
		"root file doesn't exist, project suffix exists": {
			rootProvider: func() string {
				return filepath.Join(mockRootDir, "not-exists")
			},
			projectProvider: func(s string) string {
				return filepath.Join(mockProjectDirSuffix, releaseFile)
			},
			expected: filepath.Join(mockProjectDirSuffix, releaseFile),
		},
		"root file doesn't exist, project in the middle": {
			rootProvider: func() string {
				return filepath.Join(mockRootDir, "not-exists")
			},
			projectProvider: func(s string) string {
				return filepath.Join(mockProjectDir, releaseFile)
			},
			expected: filepath.Join(mockProjectDir, releaseFile),
		},
		"no file exists": {
			rootProvider: func() string {
				return filepath.Join(mockRootDir, "not-exists")
			},
			projectProvider: func(s string) string {
				return "not-exists"
			},
			err: "failed to find release file",
		},
		"project path contain gitlab-runner-operator but release.yaml doesn't exist": {
			rootProvider: func() string {
				return filepath.Join(mockRootDir, "not-exists")
			},
			projectProvider: func(s string) string {
				return "/gitlab-runner-operator"
			},
			err: "failed to find release file",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			if tt.rootProvider != nil {
				original := rootReleaseFileProvider
				rootReleaseFileProvider = tt.rootProvider
				defer func() {
					rootReleaseFileProvider = original
				}()
			}

			if tt.projectProvider != nil {
				original := projectReleaseFileProvider
				projectReleaseFileProvider = tt.projectProvider
				defer func() {
					projectReleaseFileProvider = original
				}()
			}

			file, err := findReleaseFile()
			require.Equal(t, tt.expected, file)
			if tt.err != "" {
				require.Errorf(t, err, tt.err)
			}
		})
	}
}

func TestGetImageURL(t *testing.T) {
	r := &releaseDirImageResolver{}

	tests := map[string]struct {
		release  *Release
		image    imageName
		expected string
		err      string
	}{
		"runner image": {
			release: &Release{
				Images: []Image{{
					Name:     string(GitLabRunnerImage),
					Upstream: "runner-image",
				}},
			},
			image:    GitLabRunnerImage,
			expected: "runner-image",
		},
		"helper image": {
			release: &Release{
				Images: []Image{{
					Name:     string(GitLabRunnerHelperImage),
					Upstream: "runner-helper",
				}},
			},
			image:    GitLabRunnerHelperImage,
			expected: "runner-helper",
		},
		"no match": {
			release: &Release{
				Images: []Image{{
					Name:     string(GitLabRunnerHelperImage),
					Upstream: "runner-helper",
				}},
			},
			image: imageName("not-exists"),
			err:   "not found in release file",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			rootReleasePath := setupRootReleaseFile(t, tt.release)

			original := rootReleaseFileProvider
			defer func() {
				rootReleaseFileProvider = original
			}()
			rootReleaseFileProvider = func() string {
				return rootReleasePath
			}

			url, err := r.getImageURL(tt.image)
			require.Equal(t, tt.expected, url)
			if tt.err != "" {
				require.Errorf(t, err, tt.err)
			}
		})
	}
}
