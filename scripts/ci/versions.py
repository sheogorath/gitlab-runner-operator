#!/usr/bin/env python3

import sys
import semver
import functools
import subprocess
import config


def versions_list():
    git = subprocess.run(['git', 'tag', '--list'], stdout=subprocess.PIPE)
    git.check_returncode()
    return [v for v in git.stdout.decode('utf-8').split('\n') if v]


def sort(versions=versions_list()):
    versions.sort(key=functools.cmp_to_key(
        lambda a, b: semver.compare(a[1:], b[1:])), reverse=True)

    return versions


def previous():
    if config.previous_version():
        return config.previous_version()

    versions = sort(list(filter(lambda v: "rc" not in v, versions_list())))
    if len(versions) > 1:
        return versions[1]
    elif len(versions) == 1:
        return versions[0]
    else:
        return "v0.0.1"


if __name__ == '__main__':
    cmd = sys.argv[1]

    if cmd == "current":
        print(sort()[0])
    elif cmd == "previous":
        print(previous())
