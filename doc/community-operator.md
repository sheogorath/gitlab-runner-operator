# Community operator

**First follow the instructions in [./building.md](./building.md) to build all the needed images and bundle!**

Community operators are uncertified operators that appear on OperatorHub.io, OpenShift Container Platform and OKD.

For more info visit [operator-framework/community-operators](https://github.com/operator-framework/community-operators).

The community operator should be released along with the official Runner operator version. Currently, there's no automatic way to do that. Follow the instructions bellow.

# Releasing the Community operator

1. Run the following command:

    ```bash
    make PREV_VERSION=1.1.0 VERSION=1.6.0 community-operator
    ```

1. Fork the [community-operators repo](https://github.com/operator-framework/community-operators)
1. Copy the contents of `community-operator/{VERSION}` to `your-community-operators-fork/community-operators/gitlab-runner-operator/{VERSION}`
1. Follow the [release instructions](https://operator-framework.github.io/community-operators/)